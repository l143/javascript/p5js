class Walker{
  constructor(x, y) {
    this.pos = createVector(x, y);
  }
  
  update() {
    const {x, y} = this.pos
    this.pos.x = x + random(-1, 1)
    this.pos.y = y + random(-1, 1)
  }
  
  show() {
    const {x, y} = this.pos
    stroke(255, 255)
    strokeWeight(6)
    point(x, y)
    // console.log(x, y)
  }
  
}