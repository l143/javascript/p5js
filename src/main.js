let walker

const [W,H] = [window.innerWidth, window.innerHeight]

function setup() {
  walker = new Walker(0, 0)
  createCanvas(W, H);
}


function draw() {
  translate(W/2, H/2)
  background(0);
  walker.update()
  walker.show()
}