class Particle {
  constructor(x, y) {
    this.pos = createVector(x, y)

    this.vel = p5.Vector.random2D()
      .mult(random(6, 8))
      // .setMag(6)
    this.acc = createVector(0, 1)
      .setMag(0.1)
  }

  update() {
    this.vel.add(this.acc)
    this.pos.add(this.vel)
  }

  show() {
    const {x, y} = this.pos
    stroke(255)
    strokeWeight(2)
    fill(255, 100)
    ellipse(x, y, 5, 5)
  }
}