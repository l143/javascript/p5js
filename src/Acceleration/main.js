const [W,H] = [window.innerWidth, window.innerHeight]

let particles = new Array(60)

function setup() {
  createCanvas(W, H);
  background(0);

  for(let i = 0; i < particles.length; i++) {
    particles[i] = new Particle(0, 0)
  }

}

function draw() {
  translate(W/2, H/2)
  background(0);

  particles.forEach((p) => {
    p.update()
    p.show()
  })
  
}