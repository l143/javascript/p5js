let walker

const [W,H] = [window.innerWidth, window.innerHeight]

function setup() {
  createCanvas(W, H);
  background(0);
}


function draw() {
  translate(W/2, H/2)
  const v = new RandomVector(255, true)
  v.show()
}