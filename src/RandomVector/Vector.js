class RandomVector {
  
  constructor(color=255, uniform=false) {
    this.color = color
    if (uniform) {
      this.v = p5.Vector.random2D()
      this.v.mult(random(50, 100))
    } else {
      this.v = createVector(
        random(-100,100),
        random(-100,100)
      )
    }
  }

  show() {
    const {x, y} = this.v

    strokeWeight(2)
    stroke(this.color)
    line(0,0, x, y)
  }
}